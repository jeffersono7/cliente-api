CREATE SEQUENCE ${schema}.hibernate_sequence START 1;

CREATE TABLE ${schema}.cliente (
    id serial not null,
    nome varchar(300) not null,
    cpf varchar(11) not null unique,
    data_nascimento timestamp not null,
    created_at timestamp not null default current_timestamp,
    updated_at timestamp not null default current_timestamp,
    primary key(id)
)
