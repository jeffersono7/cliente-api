package br.com.cliente.core.service;

import br.com.cliente.core.exception.BusinessException;
import br.com.cliente.core.exception.ConflictException;
import br.com.cliente.core.exception.NotFoundException;
import br.com.cliente.core.entities.Cliente;
import br.com.cliente.core.models.ClienteQuery;
import br.com.cliente.core.models.paginacao.Pagina;
import br.com.cliente.core.ports.driven.ClienteRepository;
import br.com.cliente.core.support.TestSupport;
import br.com.cliente.core.utils.Mensagem;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ClienteServiceImplTest extends TestSupport {

    @InjectMocks
    private ClienteServiceImpl clienteService;

    @Mock
    private ClienteRepository clienteRepository;

    @Test
    @SneakyThrows
    void criar_dadoClienteValido_quandoTentarCriar_deveRetornarClienteSalvo() {
        var dataNascimento = LocalDate.of(1999, 12, 1);
        var cliente = Cliente.builder()
                .cpf(CPF)
                .nome("testador java")
                .dataNascimento(dataNascimento)
                .build();
        when(clienteRepository.salvar(cliente)).thenReturn(mockClienteSalvo(cliente));
        when(clienteRepository.isCpfCadastrado(CPF)).thenReturn(Boolean.FALSE);

        var result = clienteService.criar(cliente);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(CPF, result.getCpf());
        assertEquals("testador java", result.getNome());
        assertEquals(dataNascimento, result.getDataNascimento());
    }

    @Test
    @SneakyThrows
    void criar_dadoQueCPFJaExisteCadastrado_quandoTentarCriar_deveLancarBusinessException() {
        var dataNascimento = LocalDate.of(1999, 12, 1);
        var cliente = Cliente.builder()
                .cpf(CPF)
                .nome("testador java")
                .dataNascimento(dataNascimento)
                .build();
        when(clienteRepository.isCpfCadastrado(CPF)).thenReturn(Boolean.TRUE);

        try {
            clienteService.criar(cliente);
            fail(DEVERIA_LANCAR_EXCEPTION);
        } catch (ConflictException e) {
            assertEquals(Mensagem.CPF_JA_CADASTRADO, e.getMensagem());
        }
    }

    @Test
    void alterar_quandoParametrosValidos_deveAlterarCliente() {
        var dataNascimento = LocalDate.of(1999, 12, 1);
        var cliente = Cliente.builder()
                .id(1l)
                .cpf(CPF)
                .nome("testador java")
                .dataNascimento(dataNascimento)
                .build();

        when(clienteRepository.obterPor(any())).thenReturn(Optional.of(cliente));
        when(clienteRepository.alterar(cliente)).thenReturn(cliente);

        var result = clienteService.alterar(cliente);

        assertNotNull(result);
        assertEquals(cliente.getId(), result.getId());
    }

    @Test
    void alterar_quandoIdForNulo_deveLancarException() {
        var dataNascimento = LocalDate.of(1999, 12, 1);
        var cliente = Cliente.builder()
                .cpf(CPF)
                .nome("testador java")
                .dataNascimento(dataNascimento)
                .build();

        try {
            var result = clienteService.alterar(cliente);
            fail(DEVERIA_LANCAR_EXCEPTION);
        } catch (BusinessException e) {
            assertEquals(Mensagem.DEVE_SER_INFORMADO_ID, e.getMensagem());
        }
    }

    @Test
    void alterar_dadoQueClienteNaoEhCadatrado_quandoTentarAlterar_deveLancarException() {
        var dataNascimento = LocalDate.of(1999, 12, 1);
        var cliente = Cliente.builder()
                .id(1l)
                .cpf(CPF)
                .nome("testador java")
                .dataNascimento(dataNascimento)
                .build();

        when(clienteRepository.obterPor(any())).thenReturn(Optional.empty());

        try {
            clienteService.alterar(cliente);
            fail(DEVERIA_LANCAR_EXCEPTION);
        } catch (NotFoundException e) {
            assertEquals(Mensagem.CLIENTE_NAO_EXISTE, e.getMensagem());
        }
    }

    @Test
    void deletar_quandoParametroValidoEClienteExiste_deveDeletarComSucesso() {

        when(clienteRepository.isClienteExiste(1l)).thenReturn(Boolean.TRUE);

        try {
            clienteService.deletar(1l);
        } catch (BusinessException e) {
            fail("Não deveria lançar exception!");
        }
    }

    @Test
    void deletar_dadoQueClienteNaoExiste_quandoTentarDeletar_deveLancarException() {
        when(clienteRepository.isClienteExiste(1l)).thenReturn(Boolean.FALSE);

        try {
            clienteService.deletar(1l);
            fail(DEVERIA_LANCAR_EXCEPTION);
        } catch (NotFoundException e) {
            assertEquals(Mensagem.CLIENTE_NAO_EXISTE, e.getMensagem());
        }
    }

    @Test
    void obter_quandoClienteExistirDeveRetornarCliente() {
        var cliente = Cliente.builder()
                .id(1l)
                .build();

        when(clienteRepository.obterPor(cliente.getId()))
                .thenReturn(Optional.of(cliente));

        var result = clienteService.obter(1l);

        assertNotNull(result);
        assertNotNull(result.getId());
    }

    @Test
    void obter_quandoClienteNaoExistirDeveLancarException() {
        when(clienteRepository.obterPor(any())).thenReturn(Optional.empty());

        try {
            clienteService.obter(1l);
            fail(DEVERIA_LANCAR_EXCEPTION);
        } catch (NotFoundException e) {
            assertEquals(Mensagem.CLIENTE_NAO_EXISTE, e.getMensagem());
        }
    }

    @Test
    void listar_deveListar() {
        var query = ClienteQuery.builder().build();

        Pagina<Cliente> pagina = Pagina.<Cliente>builder()
                .paginacao(Pagina.MetadadoPagina.builder().build())
                .resultado(Collections.emptyList())
                .build();

        when(clienteRepository.listar(query)).thenReturn(pagina);

        var result = clienteService.listar(query);

        assertNotNull(result);
    }

    private Cliente mockClienteSalvo(Cliente cliente) {
        return Cliente.builder()
                .id(1l)
                .nome(cliente.getNome())
                .cpf(cliente.getCpf())
                .dataNascimento(cliente.getDataNascimento())
                .build();
    }
}