package br.com.cliente.core.entities.domain;

import br.com.cliente.core.entities.Cliente;
import br.com.cliente.core.support.TestSupport;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ClienteTest extends TestSupport {

    @Test
    void deveRetornarIdadeCalculada() {
        Map<LocalDate, Integer> dataNascimentoIdade = new HashMap<>();
        dataNascimentoIdade.put(LocalDate.now().minusYears(1), 1);
        dataNascimentoIdade.put(LocalDate.now().minusYears(25), 25);
        dataNascimentoIdade.put(LocalDate.now().minusYears(67), 67);
        dataNascimentoIdade.put(LocalDate.now().minusYears(5), 5);
        dataNascimentoIdade.put(LocalDate.of(1995, 7,12), 25);

        dataNascimentoIdade.forEach((data, idade) -> {
            var cliente = Cliente.builder().dataNascimento(data).build();
            assertEquals(idade, cliente.getIdade());
        });
    }

    @Test
    void quandoDataNascimentoNulaDeveRetornarNulo() {
        var cliente = Cliente.builder().build();

        assertNull(cliente.getIdade());
    }
}