package br.com.cliente.core.support;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public abstract class TestSupport {
    public static final String CPF = "49008655080";
    public static final String DEVERIA_LANCAR_EXCEPTION = "Deveria lançar exception!";
}
