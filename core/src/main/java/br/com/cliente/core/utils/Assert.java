package br.com.cliente.core.utils;

import br.com.cliente.core.exception.BusinessException;
import br.com.cliente.core.exception.ConflictException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Assert {

    public static void assertTrue(Boolean param, Mensagem mensagem) {
        if (!param.booleanValue()) {
            throw new BusinessException(mensagem);
        }
    }

    public static void assertFalse(Boolean param, Mensagem mensagem) {
        if (param.booleanValue()) {
            throw new BusinessException(mensagem);
        }
    }

    public static void assertFalse(Boolean param, Mensagem mensagem, Boolean isConflict) {
        if (!param.booleanValue()) {
            return;
        }

        if (isConflict) {
            throw new ConflictException(mensagem);
        }
        throw new BusinessException(mensagem);
    }

    public static void assertEquals(Object expected, Object atual, Mensagem mensagem) {
        if (!expected.equals(atual)) {
            throw new BusinessException(mensagem);
        }
    }

    public static void assertNotNull(Object evaluate, Mensagem mensagem) {
        if (Objects.isNull(evaluate)) {
            throw new BusinessException(mensagem);
        }
    }
}
