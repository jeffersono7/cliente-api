package br.com.cliente.core.models.paginacao;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public interface PageableUtils {
    Integer TAMANHO_PAGE_DEFAULT = 10;

    Integer getPagina();

    Integer getTamanho();

    Sort.Direction getOrdemDirecao();

    String getOrdemPor();

    default Pageable getPageable() {
        var sort = getSort();

        if (sort != null) {
            return PageRequest.of(
                    getPagina() != null ? getPagina() : 0,
                    getTamanho() != null ? getTamanho() : TAMANHO_PAGE_DEFAULT,
                    getSort()
            );
        }
        return PageRequest.of(
                getPagina() != null ? getPagina() : 0,
                getTamanho() != null ? getTamanho() : TAMANHO_PAGE_DEFAULT
        );
    }

    private Sort getSort() {
        if (getOrdemDirecao() == null || getOrdemPor() == null) {
            return null;
        }
        return Sort.by(getOrdemDirecao(), getOrdemPor());
    }
}
