package br.com.cliente.core.exception;

import br.com.cliente.core.utils.Mensagem;
import lombok.NoArgsConstructor;

import java.util.function.Supplier;

@NoArgsConstructor
public class ConflictException extends BusinessException {

    public ConflictException(Mensagem mensagem) {
        super(mensagem);
    }

    public static Supplier<ConflictException> supplier(Mensagem mensagem) {
        return () -> new ConflictException(mensagem);
    }
}
