package br.com.cliente.core.entities.predicates;

import br.com.cliente.core.models.utils.SearchCriteria;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@AllArgsConstructor
public class BasicPredicateBuilder<T> {
    private static final String PATTERN = "([\\w.]+?)([:<>])([\\w.\\- ]+)";

    @Getter
    private final List<SearchCriteria> criterias = new ArrayList<>();
    private final Class<T> tClass;
    private final String collectionName;

    public BasicPredicateBuilder from(String query) throws IllegalArgumentException {
        if (query == null) return this;

        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(query + ",");

        while (matcher.find()) {
            var criteria = new SearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3));

            this.criterias.add(criteria);
        }
        if (criterias.isEmpty()) {
            throw new IllegalArgumentException("Invalid query: " + query);
        }

        return this;
    }

    public void with(String key, String operation, String value) {
        var criteria = new SearchCriteria(key, operation, value);

        if (criteria.isValid()) {
            this.criterias.add(criteria);
        }
    }

    public BooleanExpression build() {
        if (criterias.isEmpty()) {
            return null;
        }

        var predicates = criterias
                .stream()
                .map(criteria -> {
                    BasicPredicate<T> predicate = new BasicPredicate<>(criteria);
                    return predicate.getPredicate(this.tClass, this.collectionName);
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        BooleanExpression expression = predicates.remove(0);

        for (BooleanExpression item : predicates) {
            expression = expression.and(item);
        }
        return expression;
    }
}
