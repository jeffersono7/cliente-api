package br.com.cliente.core.service;

import br.com.cliente.core.exception.NotFoundException;
import br.com.cliente.core.entities.Cliente;
import br.com.cliente.core.models.ClienteQuery;
import br.com.cliente.core.models.paginacao.Pagina;
import br.com.cliente.core.ports.driven.ClienteRepository;
import br.com.cliente.core.ports.driver.ClienteService;
import br.com.cliente.core.utils.Assert;
import br.com.cliente.core.utils.Mensagem;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class ClienteServiceImpl implements ClienteService {

    private final ClienteRepository clienteRepository;

    @Override
    @Transactional
    public Cliente criar(Cliente cliente) {
        var cpf = cliente.getCpf();
        Assert.assertFalse(clienteRepository.isCpfCadastrado(cpf), Mensagem.CPF_JA_CADASTRADO, true);

        return clienteRepository.salvar(cliente);
    }

    @Override
    @Transactional
    public Cliente alterar(@NotNull @Valid Cliente novo) {
        Assert.assertNotNull(novo.getId(), Mensagem.DEVE_SER_INFORMADO_ID);

        return clienteRepository.obterPor(novo.getId())
                .map(persistente -> alterarPersistente(persistente, novo))
                .map(clienteRepository::alterar)
                .orElseThrow(NotFoundException.supplier(Mensagem.CLIENTE_NAO_EXISTE));
    }

    @Override
    @Transactional
    public void deletar(@NotNull Long id) {
        assertThatClienteExiste(id);

        clienteRepository.deletar(id);
    }

    @Override
    public Cliente obter(@NotNull Long id) {
        return clienteRepository.obterPor(id)
                .orElseThrow(NotFoundException.supplier(Mensagem.CLIENTE_NAO_EXISTE));
    }

    @Override
    public Pagina<Cliente> listar(ClienteQuery query) {
        return clienteRepository.listar(query);
    }

    // private methods
    private void assertThatClienteExiste(Long id) {
        if (!clienteRepository.isClienteExiste(id)) {
            throw new NotFoundException(Mensagem.CLIENTE_NAO_EXISTE);
        }
    }


    private Cliente alterarPersistente(Cliente persistente, Cliente novo) {
        persistente.setNome(novo.getNome());
        persistente.setCpf(novo.getCpf());
        persistente.setDataNascimento(novo.getDataNascimento());

        return persistente;
    }
}
