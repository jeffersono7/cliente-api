package br.com.cliente.core.ports.driver;

import br.com.cliente.core.entities.Cliente;
import br.com.cliente.core.models.ClienteQuery;
import br.com.cliente.core.models.paginacao.Pagina;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
public interface ClienteService {

    Cliente criar(@NotNull @Valid Cliente cliente);

    Cliente alterar(@NotNull @Valid Cliente cliente);

    void deletar(@NotNull Long id);

    Cliente obter(@NotNull Long id);

    Pagina<Cliente> listar(@NotNull ClienteQuery query);
}
