package br.com.cliente.core.models.paginacao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Sort;

@Getter
@Setter
public abstract class AbstractQuery implements PageableUtils {

    private String query;
    private Integer pagina;
    private Integer tamanho;
    private String ordemPor;
    private Sort.Direction ordemDirecao;
}
