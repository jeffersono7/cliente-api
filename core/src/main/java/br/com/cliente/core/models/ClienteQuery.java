package br.com.cliente.core.models;

import br.com.cliente.core.models.paginacao.AbstractQuery;
import lombok.*;


@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClienteQuery extends AbstractQuery {

    private String cpf;
    private String nome;
}
