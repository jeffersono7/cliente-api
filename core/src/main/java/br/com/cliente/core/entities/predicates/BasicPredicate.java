package br.com.cliente.core.entities.predicates;


import br.com.cliente.core.models.utils.SearchCriteria;
import com.querydsl.core.types.dsl.*;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

@AllArgsConstructor
public class BasicPredicate<T> {
    private final SearchCriteria criteria;

    public BooleanExpression getPredicate(Class<T> tClass, String collectionName) {
        PathBuilder<T> entityPath = new PathBuilder<>(tClass, collectionName);

        if (this.isDate(criteria.getValue())) {
            DatePath<LocalDate> datePath = entityPath.getDate(criteria.getKey(), LocalDate.class);
            var dateValue = LocalDate.parse(criteria.getValue());

            switch (criteria.getOperation()) {
                case ":":
                    return datePath.eq(dateValue);
                case ">":
                    return datePath.gt(dateValue);
                case "<":
                    return datePath.lt(dateValue);
            }
        }

        if (this.isNumber(criteria.getValue())) {
            NumberPath<BigDecimal> numberPath = entityPath.getNumber(criteria.getKey(), BigDecimal.class);
            var numberValue = new BigDecimal(criteria.getValue());

            switch (criteria.getOperation()) {
                case ":":
                    return numberPath.eq(numberValue);
                case ">":
                    return numberPath.gt(numberValue);
                case "<":
                    return numberPath.lt(numberValue);
            }
        }

        StringPath stringPath = entityPath.getString(criteria.getKey());

        if (criteria.getOperation().equalsIgnoreCase(":")) {
            return stringPath.containsIgnoreCase(criteria.getValue());
        }

        return null;
    }

    private boolean isDate(String value) {
        try {
            LocalDate.parse(value);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }

    private boolean isNumber(String value) {
        try {
            new BigDecimal(value);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
