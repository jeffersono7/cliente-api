package br.com.cliente.core.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Mensagem {
    CPF_JA_CADASTRADO("CPF j\u00E1 est\u00E1 cadastrado!"),
    DEVE_SER_INFORMADO_ID("Deve ser informado ID!"),
    CLIENTE_NAO_EXISTE("Cliente n\u00E3o existe!");

    private String descricao;
}
