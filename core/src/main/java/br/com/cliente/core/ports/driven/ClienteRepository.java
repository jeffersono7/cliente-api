package br.com.cliente.core.ports.driven;

import br.com.cliente.core.entities.Cliente;
import br.com.cliente.core.entities.predicates.ClientePredicateBuilder;
import br.com.cliente.core.models.ClienteQuery;
import br.com.cliente.core.models.paginacao.Pagina;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Validated
public interface ClienteRepository {

    Cliente salvar(@NotNull Cliente cliente);

    Boolean isCpfCadastrado(@NotBlank String cpf);

    Optional<Cliente> obterPor(Long id);

    Boolean isClienteExiste(Long id);

    Cliente alterar(Cliente cliente);

    void deletar(Long id);

    Pagina<Cliente> listar(ClienteQuery query);

    default BooleanExpression getPredicate(ClienteQuery query) {
        return ClientePredicateBuilder
                .getInstance()
                .withCpf(query.getCpf())
                .withNome(query.getNome())
                .from(query.getQuery())
                .build();
    }
}
