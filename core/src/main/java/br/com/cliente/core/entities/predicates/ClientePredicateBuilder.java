package br.com.cliente.core.entities.predicates;

import br.com.cliente.core.entities.Cliente;

public class ClientePredicateBuilder extends BasicPredicateBuilder {

    public ClientePredicateBuilder() {
        super(Cliente.class, "cliente");
    }

    public static ClientePredicateBuilder getInstance() {
        return new ClientePredicateBuilder();
    }

    public ClientePredicateBuilder withCpf(String cpf) {
        with("cpf", ":", cpf);
        return this;
    }

    public ClientePredicateBuilder withNome(String nome) {
        with("nome", ":", nome);
        return this;
    }
}
