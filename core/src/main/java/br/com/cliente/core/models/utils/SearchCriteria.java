package br.com.cliente.core.models.utils;

import lombok.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
public class SearchCriteria {
    private static final List<String> OPERATIONS = Arrays.asList("<", ">", ":");

    private String key;
    private String operation;
    private String value;

    public Boolean isValid() {
        return Objects.nonNull(key)
                && OPERATIONS.contains(operation)
                && Objects.nonNull(value);
    }

    public Boolean isInvalid() {
        return !isValid();
    }
}
