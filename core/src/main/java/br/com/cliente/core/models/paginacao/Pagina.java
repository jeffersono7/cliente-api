package br.com.cliente.core.models.paginacao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Pagina<T> {

    private List<T> resultado;
    private MetadadoPagina paginacao;

    public static <T> Pagina<T> from(Page<T> page) {
        var paginacao = MetadadoPagina
                .builder()
                .paginaAnterior(page.hasPrevious() ? page.getNumber() - 1 : null)
                .paginaAtual(page.getNumber())
                .proximaPagina(page.hasNext() ? page.getNumber() + 1 : null)
                .totalPaginas(page.getTotalPages())
                .tamanho(page.getSize())
                .totalItens(page.getTotalElements())
                .build();

        return Pagina.<T>builder()
                .resultado(page.getContent())
                .paginacao(paginacao)
                .build();
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MetadadoPagina {
        private Integer tamanho;
        private Integer paginaAnterior;
        private Integer paginaAtual;
        private Integer proximaPagina;
        private Integer totalPaginas;
        private Long totalItens;
    }
}
