package br.com.cliente.core.entities;

import lombok.*;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "cliente")
@Data
@With
@EqualsAndHashCode(of = "id")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", columnDefinition = "serial", nullable = false)
    private Long id;

    @NotBlank
    @Column(name = "nome", columnDefinition = "varchar(300)", nullable = false)
    private String nome;

    @CPF(message = "CPF inválido!")
    @NotBlank
    @Column(name = "cpf", columnDefinition = "varchar(11)", nullable = false)
    private String cpf;

    @Past
    @NotNull
    @Column(name = "data_nascimento", columnDefinition = "timestamp", nullable = false)
    private LocalDate dataNascimento;

    @Column(name = "created_at", columnDefinition = "timestamp", nullable = false)
    @CreatedDate
    private LocalDate createdAt;

    @Column(name = "updated_at", columnDefinition = "timestamp", nullable = false)
    @LastModifiedDate
    private LocalDate updatedAt;

    public Integer getIdade() {
        return Optional.ofNullable(dataNascimento)
                .map(data -> Period.between(data, LocalDate.now()).getYears())
                .orElse(null);
    }
}
