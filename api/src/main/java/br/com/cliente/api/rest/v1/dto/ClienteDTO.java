package br.com.cliente.api.rest.v1.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClienteDTO implements Serializable {
    public static long serialVersionUID = -7916474858425234287L;

    @ApiModelProperty(name = "ID do cliente")
    private Long id;

    @NotBlank
    @ApiModelProperty(name = "Nome do cliente", required = true, example = "João")
    private String nome;

    @CPF(message = "CPF inválido!")
    @NotNull
    @ApiModelProperty(name = "CPF do cliente", required = true)
    private String cpf;

    @NotNull
    @Past(message = "Data de nascimento deve ser anterior a data atual")
    @ApiModelProperty(name = "Data de nascimento do cliente", required = true)
    private LocalDate dataNascimento;

    private Integer idade;

    private LocalDate createdAt;

    private LocalDate updatedAt;
}
