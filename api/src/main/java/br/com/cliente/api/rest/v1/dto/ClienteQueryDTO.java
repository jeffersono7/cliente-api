package br.com.cliente.api.rest.v1.dto;

import lombok.*;
import org.springframework.data.domain.Sort;

import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClienteQueryDTO implements Serializable {
    public static long serialVersionUID = 6663040751130274992L;

    private String cpf;
    private String nome;
    private String query;
    private Integer pagina;
    private Integer tamanho;
    private String ordemPor;
    private Sort.Direction ordemDirecao;
}
