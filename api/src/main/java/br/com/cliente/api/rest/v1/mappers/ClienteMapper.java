package br.com.cliente.api.rest.v1.mappers;

import br.com.cliente.api.rest.v1.dto.ClienteDTO;
import br.com.cliente.api.rest.v1.dto.ClienteQueryDTO;
import br.com.cliente.api.utils.MapperConstant;
import br.com.cliente.core.entities.Cliente;
import br.com.cliente.core.models.ClienteQuery;
import br.com.cliente.core.models.paginacao.Pagina;
import org.mapstruct.*;

import java.util.stream.Collectors;

@Mapper(componentModel = MapperConstant.SPRING, builder = @Builder(disableBuilder = true))
public interface ClienteMapper {

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    Cliente fromDto(ClienteDTO cliente);

    @Mapping(target = "idade", expression = "java(cliente.getIdade())")
    ClienteDTO toDto(Cliente cliente);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    void updateClienteFromDto(@MappingTarget Cliente cliente, ClienteDTO dto);

    // query
    ClienteQuery fromDto(ClienteQueryDTO clienteQuery);

    // default methods
    default Pagina<ClienteDTO> toDto(Pagina<Cliente> clientePagina) {
        var resultado = clientePagina
                .getResultado()
                .stream()
                .map(this::toDto)
                .collect(Collectors.toList());

        return Pagina.<ClienteDTO>builder()
                .paginacao(clientePagina.getPaginacao())
                .resultado(resultado)
                .build();
    }
}
