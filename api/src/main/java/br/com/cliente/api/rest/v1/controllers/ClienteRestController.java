package br.com.cliente.api.rest.v1.controllers;

import br.com.cliente.api.config.aspect.LoggerRest;
import br.com.cliente.api.rest.v1.ClienteApi;
import br.com.cliente.api.rest.v1.dto.ClienteDTO;
import br.com.cliente.api.rest.v1.dto.ClienteQueryDTO;
import br.com.cliente.api.rest.v1.mappers.ClienteMapper;
import br.com.cliente.core.exception.InternalServerException;
import br.com.cliente.core.models.paginacao.Pagina;
import br.com.cliente.core.ports.driver.ClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
public class ClienteRestController implements ClienteApi {

    private final ClienteService clienteService;
    private final ClienteMapper clienteMapper;

    @LoggerRest
    @Override
    public ClienteDTO criar(ClienteDTO cliente) {
        return Optional.of(cliente)
                .map(clienteMapper::fromDto)
                .map(clienteService::criar)
                .map(clienteMapper::toDto)
                .orElseThrow(InternalServerException.supplier("Objeto nulo não esperado!"));
    }

    @LoggerRest
    @Override
    public ClienteDTO alterar(Long id, @NotNull @Valid ClienteDTO cliente) {
        return Optional.of(cliente)
                .map(clienteMapper::fromDto)
                .map(c -> c.withId(id))
                .map(clienteService::alterar)
                .map(clienteMapper::toDto)
                .orElseThrow(InternalServerException.supplier("Objeto nulo não esperado!"));
    }

    @LoggerRest
    @Override
    public ClienteDTO alterarParcialmente(Long id, @NotNull ClienteDTO novo) {
        var cliente = clienteService.obter(id);
        clienteMapper.updateClienteFromDto(cliente, novo);

        return Optional.of(cliente)
                .map(c -> c.withId(id))
                .map(clienteService::alterar)
                .map(clienteMapper::toDto)
                .orElseThrow(InternalServerException.supplier("Objeto nulo não esperado!"));
    }

    @LoggerRest
    @Override
    public void deletar(Long id) {
        clienteService.deletar(id);
    }

    @LoggerRest
    @Override
    public ClienteDTO obter(Long id) {
        return Optional.of(id)
                .map(clienteService::obter)
                .map(clienteMapper::toDto)
                .orElseThrow(InternalServerException.supplier("Objeto nulo não esperado!"));
    }

    @Override
    public Pagina<ClienteDTO> listar(ClienteQueryDTO clienteQuery) {
        return Optional.of(clienteQuery)
                .map(clienteMapper::fromDto)
                .map(clienteService::listar)
                .map(clienteMapper::toDto)
                .orElseThrow(InternalServerException.supplier("Objeto nulo não esperado!"));
    }
}
