package br.com.cliente.api.rest.v1;

import br.com.cliente.api.rest.v1.dto.ClienteDTO;
import br.com.cliente.api.rest.v1.dto.ClienteQueryDTO;
import br.com.cliente.core.models.ClienteQuery;
import br.com.cliente.core.models.paginacao.Pagina;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Api(value = "Serviço de clientes", tags = "v1")
@RequestMapping(
        path = ClienteApi.PATH,
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
)
public interface ClienteApi {

    String PATH = "/v1/clientes";
    String ID = "id";
    String ID_PATH = "/{" + ID + "}";

    @ApiOperation("Criar cliente")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    ClienteDTO criar(@NotNull @Valid @RequestBody ClienteDTO cliente);

    @ApiOperation("Alterar cliente")
    @PutMapping(ID_PATH)
    ClienteDTO alterar(@PathVariable(ID) Long id, @NotNull @Valid @RequestBody ClienteDTO cliente);

    @ApiOperation("Alterar parcialmente cliente")
    @PatchMapping(ID_PATH)
    ClienteDTO alterarParcialmente(@PathVariable(ID) Long id, @NotNull @RequestBody ClienteDTO cliente);

    @ApiOperation("Deletar cliente")
    @DeleteMapping(value = ID_PATH, consumes = MediaType.ALL_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deletar(@PathVariable(ID) Long id);

    @ApiOperation("Obter cliente por ID")
    @GetMapping(value = ID_PATH, consumes = MediaType.ALL_VALUE)
    ClienteDTO obter(@PathVariable(ID) Long id);

    @ApiOperation("Listar clientes")
    @GetMapping(consumes = MediaType.ALL_VALUE)
    Pagina<ClienteDTO> listar(ClienteQueryDTO clienteQuery);
}
