package br.com.cliente.api.adapter.repository.jpa;

import br.com.cliente.core.entities.Cliente;
import br.com.cliente.core.entities.QCliente;
import br.com.cliente.core.models.ClienteQuery;
import br.com.cliente.core.models.paginacao.Pagina;
import br.com.cliente.core.ports.driven.ClienteRepository;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClienteJpaRepository extends ClienteRepository,
        JpaRepository<Cliente, Long>,
        QuerydslPredicateExecutor<Cliente>,
        QuerydslBinderCustomizer<QCliente> {

    Boolean existsByCpfEquals(String cpf);

    @Override
    default Cliente salvar(Cliente cliente) {
        return save(cliente);
    }

    @Override
    default Boolean isCpfCadastrado(String cpf) {
        return existsByCpfEquals(cpf);
    }

    @Override
    default Optional<Cliente> obterPor(Long id) {
        return findById(id);
    }

    @Override
    default Boolean isClienteExiste(Long id) {
        return existsById(id);
    }

    @Override
    default Cliente alterar(Cliente cliente) {
        return save(cliente);
    }

    @Override
    default void deletar(Long id) {
        deleteById(id);
    }

    @Override
    default Pagina<Cliente> listar(ClienteQuery query) {
        var pageable = query.getPageable();

        return Optional.ofNullable(query)
                .map(this::getPredicate)
                .map(predicate -> this.findAll(predicate, pageable))
                .map(Pagina::from)
                .orElseGet(() -> {
                    var page = findAll(pageable);
                    return Pagina.from(page);
                });
    }

    @Override
    default void customize(QuerydslBindings bindings, QCliente qCliente) {
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
