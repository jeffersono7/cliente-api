package br.com.cliente.api.rest.v1.controllers;

import br.com.cliente.api.rest.v1.ClienteApi;
import br.com.cliente.api.rest.v1.dto.ClienteDTO;
import br.com.cliente.api.support.ITSupport;
import br.com.cliente.core.entities.Cliente;
import br.com.cliente.core.models.paginacao.Pagina;
import br.com.cliente.core.ports.driven.ClienteRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Rollback
class ClienteRestControllerIT implements ITSupport {

    @Getter
    @Autowired
    private MockMvc mockMvc;

    @Getter
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ClienteRepository clienteRepository;

    @Test
    @SneakyThrows
    @Rollback
    public void criar_quandoParametrosValidos_deveCriarCliente() {
        var cliente = ClienteDTO.builder()
                .nome("testador java")
                .cpf(CPF_GERADO2)
                .dataNascimento(LocalDate.of(2000, 1, 1))
                .build();

        var clienteSalvo = post(ClienteApi.PATH, cliente, status().isCreated(), ClienteDTO.class);

        assertNotNull(clienteSalvo);
        assertNotNull(clienteSalvo.getId());
        assertNotNull(clienteSalvo.getCpf());
    }

    @Test
    @SneakyThrows
    @Rollback
    public void criar_quandoParametrosValidosMasCPFJaEhCadastrado_deveRetornarError() {
        var cliente = ClienteDTO.builder()
                .nome("testador java")
                .cpf(CPF_GERADO)
                .dataNascimento(LocalDate.of(2000, 1, 1))
                .build();

        var clienteSalvo = post(ClienteApi.PATH, cliente, status().isCreated(), ClienteDTO.class);

        assertNotNull(clienteSalvo);
        assertNotNull(clienteSalvo.getId());
        assertNotNull(clienteSalvo.getCpf());

        post(ClienteApi.PATH, cliente, status().isConflict(), String.class);
    }

    @Test
    @SneakyThrows
    public void criar_quandoParametrosInvalidos_deveRetornarErros() {
        var cliente = ClienteDTO.builder().build();

        post(ClienteApi.PATH, cliente, status().isBadRequest(), String.class);
    }

    @Test
    @SneakyThrows
    @Rollback
    public void alterar_quandoParametrosValidosEClienteExiste_deveAlterarCliente() {
        var clienteSave = Cliente.builder()
                .nome("testador")
                .cpf(CPF_GERADO)
                .dataNascimento(LocalDate.of(1999, 3, 4))
                .build();
        clienteSave = clienteRepository.salvar(clienteSave);

        var cliente = ClienteDTO.builder()
                .id(clienteSave.getId())
                .cpf(CPF_GERADO)
                .nome("testador")
                .dataNascimento(LocalDate.of(1988, 1, 1))
                .build();

        String path = ClienteApi.PATH + ClienteApi.ID_PATH.replace("{id}", cliente.getId().toString());

        var result = put(
                path,
                cliente,
                status().isOk(),
                ClienteDTO.class);

        assertNotNull(result);
    }

    @Test
    @SneakyThrows
    void alterar_quandoClienteNaoForCadatrado_deveRetornarError() {

        var cliente = ClienteDTO.builder()
                .id(1l)
                .cpf(CPF_GERADO)
                .nome("testador")
                .dataNascimento(LocalDate.of(1988, 1, 1))
                .build();

        String path = ClienteApi.PATH + ClienteApi.ID_PATH.replace("{id}", cliente.getId().toString());

        put(path, cliente, status().isNotFound(), String.class);
    }

    @Test
    void alterar_quandoParametrosInvalidos_deveRetornarError() {
        var cliente = ClienteDTO.builder().build();

        String path = ClienteApi.PATH + ClienteApi.ID_PATH.replace("{id}", "1");

        put(path, cliente, status().isBadRequest(), String.class);
    }

    @Test
    @Rollback
    void alterarParcialmente_quandoClienteExistir_deveAlterar() {
        var clienteSave = Cliente.builder()
                .nome("testador")
                .cpf(CPF_GERADO)
                .dataNascimento(LocalDate.of(1999, 3, 4))
                .build();
        clienteSave = clienteRepository.salvar(clienteSave);

        var cliente = ClienteDTO.builder()
                .nome("outro nome")
                .build();

        String path = ClienteApi.PATH + ClienteApi.ID_PATH.replace("{id}", clienteSave.getId().toString());

        var result = patch(
                path,
                cliente,
                status().isOk(),
                ClienteDTO.class);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals("outro nome", result.getNome());
    }

    @Test
    @Rollback
    void deletar_quandoClienteExistir_deveDeletar() {
        var clienteSave = Cliente.builder()
                .nome("testador")
                .cpf(CPF_GERADO)
                .dataNascimento(LocalDate.of(1999, 3, 4))
                .build();
        clienteSave = clienteRepository.salvar(clienteSave);

        String path = ClienteApi.PATH + ClienteApi.ID_PATH.replace("{id}", clienteSave.getId().toString());

        delete(path, status().isNoContent());
    }

    @Test
    void deletar_quandoClienteNaoExistir_deveRetornarErro() {

        String path = ClienteApi.PATH + ClienteApi.ID_PATH.replace("{id}", "10");

        delete(path, status().isNotFound());
    }

    @Test
    @Rollback
    void obter_quandoClienteExistir_deveRetornarEle() {
        var clienteSave = Cliente.builder()
                .nome("testador")
                .cpf(CPF_GERADO)
                .dataNascimento(LocalDate.of(1999, 3, 4))
                .build();
        clienteSave = clienteRepository.salvar(clienteSave);

        String path = ClienteApi.PATH + ClienteApi.ID_PATH.replace("{id}", clienteSave.getId().toString());

        var result = get(path, status().isOk(), ClienteDTO.class);

        assertNotNull(result);
        assertEquals(clienteSave.getId(), result.getId());
        assertEquals(clienteSave.getCpf(), result.getCpf());
        assertEquals(clienteSave.getNome(), result.getNome());
        assertEquals(clienteSave.getDataNascimento(), result.getDataNascimento());
    }

    @Test
    void obter_quandoClienteNaoExistir_deveRetornarError() {

        String path = ClienteApi.PATH + ClienteApi.ID_PATH.replace("{id}", "100");

        get(path, status().isNotFound(), String.class);
    }

    @Test
    @Rollback
    void listar_deveListar() {
        var clienteSave = Cliente.builder()
                .nome("testador")
                .cpf(CPF_GERADO)
                .dataNascimento(LocalDate.of(1999, 3, 4))
                .build();
        clienteRepository.salvar(clienteSave);

        String path = ClienteApi.PATH;

        var result = (Pagina<Cliente>) get(path, status().isOk(), Pagina.class);

        assertNotNull(result);
        assertNotNull(result.getResultado());
        assertEquals(1, result.getResultado().size());
        assertNotNull(result.getPaginacao());
        assertEquals(1, result.getPaginacao().getTotalItens());
    }

    @Test
    @Rollback
    void listar_deveRetornarPaginaVazia() {
        String path = ClienteApi.PATH;

        var result = (Pagina<Cliente>) get(path, status().isOk(), Pagina.class);

        assertNotNull(result);
        assertNotNull(result.getPaginacao());
        assertNotNull(result.getResultado());
        assertTrue(result.getResultado().isEmpty());
        assertEquals(0, result.getPaginacao().getTotalPaginas());
    }
}
